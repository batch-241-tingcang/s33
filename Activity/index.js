//Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json));


// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
  });


// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
  .then(response => response.json())
  .then(data => {
    console.log(`Title: ${data.title}, Status: ${data.completed}`);
  });



// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		"userId": 1,
		"title": "Create Post",
		"body": "Create Post File"
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Mica title",
		"completed": false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"title": "Mica title - edited",
		"completed": true
	})
})
.then((response) => response.json())
.then((json) => console.log(json));



// Update a to do list item by changing the data structure to contain the following properties:
// a. Title
// b. Description
// c. Status
// d. Date Completed
// e. User ID

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"title": "Mica title new",
		"description": "Updated the title",
		"status": "pending",
		"dateCompleted": "2023-01-30",
		"userId": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"title": "Mica title new",
		"description": "Updated the title",
		"status": "pending",
		"dateCompleted": "2023-01-30",
		"userId": 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"status": "complete",
		"dateCompleted": "2023-01-31"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE'
});



