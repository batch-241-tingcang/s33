// console.log('Hello world');
// conosle.log("Hi B241!");
// console.log("Hi B241!");


// [SECTION] Javascript Synchronous vs Asynchronous
// Aynchronous means that we can proceed to execute other statements, while time consuming code is running in the background.


// Fetch() method returns a promise that resolves to a response object
// promise - is an object that represents the eventual completion (or failure) of an aysnchronous function and its resulting value
console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));

// Fetch() method returns a promise that resolves to a "response object"
fetch('https://jsonplaceholder.typicode.com/posts/')
// then() method captures the "response object" and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts/')
// "json()" - from the "response object" to convert the data retrieved into JSON format to be used in our application 
.then(response => response.json())
// Print the converetd JSON value from the "fetch" request
.then((json) => console.log(json));
// "promise chain" - using multiple .then()


//  async and await

async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	let json = await result.json();
	console.log(json);
}
fetchData();

// [SECTION] Creating a post

fetch('https://jsonplaceholder.typicode.com/posts', {
	// Sets the method of the "request object"
	method: 'POST',
	// Sets the header data of the "request object" to be sent to the backedn
	headers: {
		'Content-type': 'application/json'
	},

	// JSON.stringify - converts the object data into a stringified JSON
	body: JSON.stringify({
		"userId": 1,
		"title": "Create Post",
		"body": "Create Post File"
	})
})

.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
		"title": "Update Post",
		"body": "Update Post File"
	})
})


.then((response) => response.json())
.then((json) => console.log(json));
































